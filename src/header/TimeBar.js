import FavIconView from "../content/FavIconView";
import "./TimeBar.css";
import { HiFire, HiOutlineHeart } from "react-icons/hi";
import { useEffect, useState } from "react";

function TimeBar(props) {
  // let test = 0;
  const [time, setTime] = useState(0);
  const [running, setRunning] = useState(false);
  const [minutes, setMinutes] = useState(0);
  const [heartRateVal, setHeartRateVal] = useState(0);
  const [caloriesVal, setCaloriesVal] = useState(0);
  useEffect(() => {
    let interval;
    if (props.state) {
      interval = setInterval(() => {
        setTime((prevTime) => prevTime + 10);
      }, 10);
      // calRates();
    } else if (!props.state) {
      clearInterval(interval);
    }
    return () => clearInterval(interval);
  }, [props.state]);

  useEffect(() => {});

  useEffect(() => {
    let val;
    if (running) {
      // setHeartRateVal(60);
      val = setInterval(() => {
        setHeartRateVal((prevHR) => prevHR + 1);
        setCaloriesVal((prevCal) => prevCal + 1);
      }, 10000);
    } else if (!running) {
      clearInterval(val);
    }
    return () => clearInterval(val);
  }, [running]);

  useEffect(() => {
    props.callTimeBarFunc.current = setValToZero;
  }, []);

  function caloriesCalc() {
    const wkg = 58.967;
    let hr = 0;
    let min = 120;
    // let sec = 5;
    const timeVal = min / 60;
    const kmh = 5 / timeVal;

    const cals =
      (0.0215 * Math.pow(kmh, 3) -
        0.1765 * Math.pow(kmh, 2) +
        0.871 * kmh +
        1.4577) *
      wkg *
      timeVal;
    return cals.toFixed(2);
  }

  function heartRate() {
    // return val;
  }

  function setValToZero() {
    setTime(0);
    setHeartRateVal(0);
    setCaloriesVal(0);
  }

  function getMinute(value) {
    return Math.floor((time / 60000) % 60).slice(-2);
  }

  return (
    <div className="counter-display">
      <div className="time-bar-first">
        <FavIconView
          title="Calories"
          iconData={
            <HiFire
              style={{ height: "50px", width: "50px", color: "#e67300" }}
            />
          }
          value={props.calories}
        />
      </div>
      <div className="time-bar-second">
        <h6 className="time-title">TRACK TIME</h6>
        <div className="numbers">
          <span className="counter">
            {("0" + Math.floor((time / 60000) % 60)).slice(-2)}:
          </span>
          <span className="counter">
            {("0" + Math.floor((time / 1000) % 60)).slice(-2)}:
          </span>
          <span className="counter">
            {("0" + ((time / 10) % 100)).slice(-2)}
          </span>
        </div>

        {/* <h2 className="counter">00:00:00</h2> */}
        {/* <div className="buttons">
          <button
            onClick={() => {
              setRunning(true);
            }}
          >
            Start
          </button>
          <button onClick={() => setRunning(false)}>Stop</button>
          <button onClick={() => setValToZero()}>Reset</button>
        </div> */}
        {/* buttons between */}
      </div>
      <div className="time-bar-third">
        <FavIconView
          title="Heart Rate"
          iconData={
            <HiOutlineHeart
              style={{ height: "50px", width: "50px", color: "red" }}
            />
          }
          value={props.heart}
        />
      </div>
    </div>
  );
}
export default TimeBar;

// function calRates() {
//   let val;
//   if (running) {
//     // setHeartRateVal(60);
//     val = setInterval(() => {
//       setCaloriesVal((prevCal) => prevCal + 1);
//     }, 10000);
//   } else if (!running) {
//     clearInterval(val);
//   }
//   return () => clearInterval(val);

//   // return val;
// }
