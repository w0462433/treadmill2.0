import "./Header.css";
import Logo from "../img/logo.png";
function Header(props) {
  return (
    <div className="header-content">
      {/* <div className="header-first">one</div> */}
      <div className="header-second">
        {props.title} <img className="header-logo" src={Logo} alt="logo" />
      </div>
      {/* <div className="header-third">third</div> */}
    </div>
  );
}
export default Header;
