import "./FavIconView.css";
function FavIconView(props) {
  return (
    <div className="fav-icon-card">
      <h6>{props.title}</h6>
      {props.iconData}
      <h4 className="data">{props.value}</h4>
    </div>
  );
}
export default FavIconView;
