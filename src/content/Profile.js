import "./Profile.css";
function Profile(props) {
  return (
    <div className="profileContainer">
      <h1>Username: {props.username}</h1>
      <h3>Time Spent: {props.time}</h3>
      <h3>Distance: {props.distance}</h3>
    </div>
  );
}
export default Profile;
