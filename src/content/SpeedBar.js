import "./SpeedBar.css";
import RadicalBar from "react-apexcharts";
function SpeedBar(props) {
  var options = {
    chart: {
      height: 280,
      type: "radialBar",
    },

    series: [props.value],

    plotOptions: {
      radialBar: {
        hollow: {
          margin: 15,
          size: "70%",
        },

        dataLabels: {
          showOn: "always",
          name: {
            offsetY: -10,
            show: true,
            color: "#e49e00",
            fontSize: "13px",
          },
          value: {
            color: "#fff",
            fontSize: "18px",
            show: true,
            formatter: function (val) {
              return val + props.unit;
            },
          },
        },
      },
    },

    stroke: {
      lineCap: "round",
    },
    labels: [props.title],
  };

  return (
    <div id="chart" className="speedbar">
      <RadicalBar
        options={options}
        series={options.series}
        type="radialBar"
        width="250"
      />
    </div>
  );
}
export default SpeedBar;
