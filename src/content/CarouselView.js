import "./CarouselView.css";
import { Carousel } from "react-bootstrap";
// import ReactPlayer from "react-player";
import "bootstrap/dist/css/bootstrap.css";
import data from "../config.json";
import ReactPlayer from "react-player";
import Player from "./Player";
import { useEffect, useRef, useState } from "react";
import pic from "../img/1.jpg";

function CarouselView() {
  const videoData = data.map((newData) => {
    // console.log(newData);
  });

  // find external source
  // function openWindow(id) {
  //   let newWindow;

  //   let val = imgProps.map((obj) => {
  //     if (obj.id == id) {
  //       let test = <ReactPlayer url={obj.src} />;
  //       newWindow = window.open(obj.src, "bigwindow"); //<Video src={obj.src}/>
  //       console.log("path: ", obj.src);
  //       // closeWindow();
  //     }
  //   });
  // }

  function newOpenWindow(id) {
    let newWindow;

    let videoData = data.map((obj) => {
      if (obj.id == id) {
        // let test = <ReactPlayer url={obj.src} />;
        newWindow = window.open(obj.src, "bigwindow"); //<Video src={obj.src}/>
        console.log("path: ", obj.src);
        // closeWindow();
      }
    });
  }

  // WIP to close every video tab that is opened
  // function closeWindow() {
  //   let videoTag = document.getElementsByTagName("video"),
  //     videoWindow;

  //   // Broadcast that you're opening a page.
  //   localStorage.openpages = Date.now();
  //   window.addEventListener(
  //     "storage",
  //     (e) => {
  //       if (e.key == "openpages") {
  //         // Listen if anybody else is opening the same page!
  //         localStorage.page_available = Date.now();
  //       }
  //       if (e.key == "page_available") {
  //         alert("One mire page already open");
  //       }
  //     },
  //     false
  //   );
  // }

  return (
    // <div className="CarouselView">
    //   <Carousel>
    //     {imgProps.map((imgObj) => {
    //       return (
    //         <Carousel.Item key={imgObj.id}>
    //           <img
    //             src={imgObj.img}
    //             onClick={() => {
    //               openWindow(imgObj.id);
    //               // console.log(videoData);
    //             }}
    //             onTouchStart={() => openWindow(imgObj.id)}
    //             alt=""
    //           />
    //           <Carousel.Caption>
    //             <h3>{imgObj.title}</h3>
    //             <p>{imgObj.credit}</p>
    //           </Carousel.Caption>
    //         </Carousel.Item>
    //       );
    //     })}
    //   </Carousel>
    // </div>
    // testing config file below
    <div className="CarouselView">
      <Carousel>
        {data.map((imgObj) => {
          return (
            <Carousel.Item
              key={imgObj.id}
              onTouchStart={() => newOpenWindow(imgObj.id)}
              onClick={() => newOpenWindow(imgObj.id)}
            >
              <img src={imgObj.img} alt="" />
              {/* <img src={pic} alt="" /> */}
              <Carousel.Caption>
                <h3>{imgObj.title}</h3>
                <p>{imgObj.credit}</p>
              </Carousel.Caption>
            </Carousel.Item>
          );
        })}
      </Carousel>
    </div>
  );

  // return (
  //   <div className="CarouselView">
  //     <Carousel>
  //       <Carousel.Item>
  //         <img
  //           className="d-block w-100"
  //           src="https://cdn.wallpapersafari.com/90/43/ZWSq2E.jpg"
  //           alt="First slide"
  //         />
  //         <Carousel.Caption>
  //           <h3>First slide label</h3>
  //           <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
  //         </Carousel.Caption>
  //       </Carousel.Item>
  //       <Carousel.Item>
  //         <img
  //           className="d-block w-100"
  //           src="https://cdn.wallpapersafari.com/97/4/0TrgQ6.jpg"
  //           alt="Second slide"
  //         />

  //         <Carousel.Caption>
  //           <h3>Second slide label</h3>
  //           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
  //         </Carousel.Caption>
  //       </Carousel.Item>
  //       <Carousel.Item>
  //         <img
  //           className="d-block w-100"
  //           src="https://cdn.wallpapersafari.com/46/10/etOFCV.png"
  //           alt="Third slide"
  //         />

  //         <Carousel.Caption>
  //           <h3>Third slide label</h3>
  //           <p>
  //             Praesent commodo cursus magna, vel scelerisque nisl consectetur.
  //           </p>
  //         </Carousel.Caption>
  //       </Carousel.Item>
  //     </Carousel>
  //   </div>
  // );
}
export default CarouselView;

// return (
//   <div className="videoCarousel">
//     {/* <h1>Video Carousel</h1> */}
//     <Carousel>
//       {videoProps.map((vidObj) => {
//         return (
//           <Carousel.Item key={vidObj.id}>
//             <ReactPlayer
//               url={vidObj.src}
//               width="100%"
//               pip={true}
//               controls={true}
//               playing={true}
//             />
//             <Carousel.Caption>
//               <h3>{vidObj.title}</h3>
//               <p>{vidObj.credit}</p>
//             </Carousel.Caption>
//             {window.open("www.google.com/")}
//           </Carousel.Item>
//         );
//       })}
//     </Carousel>
//   </div>
// );
