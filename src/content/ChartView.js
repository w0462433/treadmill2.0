// import { Doughnut } from 'react-chartjs-2';
import "./ChartView.css";
import pic1 from "../img/1.jpg";
// import vid from "./videos/video3.mp4";
import {
  PieChart,
  Pie,
  Sector,
  Cell,
  Tooltip,
  XAxis,
  YAxis,
  Legend,
  CartesianGrid,
  LineChart,
  Line,
  ResponsiveContainer,
  Brush,
} from "recharts";

function ChartView({ data }) {
  // console.log("chart: ", data.length);
  return (
    // <LineChart
    //   width={500}
    //   height={400}
    //   data={data}
    //   margin={{
    //     top: 5,
    //     right: 30,
    //     left: 20,
    //     bottom: 5,
    //   }}
    // >
    //   <CartesianGrid strokeDasharray="3 3" />
    //   <XAxis dataKey="name" />
    //   <YAxis />
    //   <Tooltip />
    //   <Legend />
    //   <Line
    //     type="monotone"
    //     dataKey="pv"
    //     stroke="#8884d8"
    //     activeDot={{ r: 8 }}
    //   />
    //   <Line type="monotone" dataKey="uv" stroke="#82ca9d" />
    // </LineChart>

    <ResponsiveContainer width="60%" height="100%">
      <LineChart width={500} height={300} data={data}>
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" padding={{ left: 30, right: 30 }} />
        <YAxis />
        <Tooltip />
        <Legend />
        <Line
          type="monotone"
          dataKey="km"
          stroke="#8884d8"
          activeDot={{ r: 8 }}
        />
        <Line type="monotone" dataKey="time" stroke="#82ca9d" />
      </LineChart>
    </ResponsiveContainer>
  );
}
export default ChartView;
