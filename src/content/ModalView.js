import { Button, Modal } from "react-bootstrap";
import ChartView from "./ChartView";
import "./ModalView.css";
import Profile from "./Profile";
function ModalView(props) {
  function controlModalView() {
    if (props.view === "graph") {
      console.log("modal graph");
      // <ChartView />;
    } else {
      console.log("modal profile");
      // <Profile />;
    }
  }

  return (
    <div className="modalContainer">
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Modal heading
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="modalBody">
            {/* <h4 className="test4">Centered Modal</h4>
            <p>testing this crap</p>
            testing */}
            <ChartView />
            {/* {controlModalView} */}
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Close</Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}
export default ModalView;
