// import "./Player.css";
import ReactPlayer from "react-player";
function Player(props) {
  return (
    <div className="playerContainer">
      <ReactPlayer url={props.url} />
    </div>
  );
}
export default Player;
