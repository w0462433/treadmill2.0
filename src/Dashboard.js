import { useEffect, useRef, useState } from "react";
import CarouselView from "./content/CarouselView";
import ChartView from "./content/ChartView";
import ModalView from "./content/ModalView";
import SpeedBar from "./content/SpeedBar";
import "./Dashboard.css";
import Header from "./header/Header";
import TimeBar from "./header/TimeBar";

function Dashboard() {
  const [modalShow, setModalShow] = useState(false);
  const [viewType, setViewtype] = useState("");
  const [values, setValues] = useState(0);
  const [speed, setSpeed] = useState(0);
  const [distance, setDistance] = useState(0);
  const [running, setRunning] = useState(false);
  const [time, setTime] = useState(0);
  const [chartData, setChartData] = useState([]);
  const [heartRateVal, setHeartRateVal] = useState(0);
  const [caloriesVal, setCaloriesVal] = useState(0);

  const callTimeBarFunc = useRef(null);

  // function controlModal(value) {
  //   if (value == "graph") {
  //     // console.log("graph");
  //     setViewtype(value);
  //     setModalShow(true);
  //   } else {
  //     // console.log("profile");
  //     setViewtype(value);
  //     setModalShow(true);
  //   }
  // }

  // useEffect(() => {
  //   let interval;
  //   if (running) {
  //     interval = setInterval(() => {
  //       setTime((prevTime) => prevTime + 10);
  //     }, 10);
  //     // calRates();
  //   } else if (!running) {
  //     clearInterval(interval);
  //   }
  //   return () => clearInterval(interval);
  // }, [running]);

  const randNumber = (start, end) => {
    return Math.floor(Math.random() * (end - start + 1) + start);
  };

  const reset = () => {
    callTimeBarFunc.current();
    setChartData([]);
    setTime(0);
    setDistance(0);
    setSpeed(0);
    setHeartRateVal(0);
    setCaloriesVal(0);
    // buttonControls("profile");
  };

  useEffect(() => {
    let interval;
    if (running) {
      interval = setInterval(() => {
        const now = new Date();
        const dt = `${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`;
        const data = {
          km: randNumber(1, 1000),
          time: randNumber(1, 1000),
          name: dt,
        };
        setChartData((prev) => {
          prev = [...prev, data];
          if (prev.length > 30) {
            prev.shift();
          }
          return prev;
        });

        setDistance((prev) => prev + 1);
        setSpeed((prev) => randNumber(5, 30));
        setHeartRateVal((prev) => randNumber(60, 120));
        setCaloriesVal((prev) => 0.5 + prev);
      }, 1000);
    }
    return () => clearInterval(interval);
  }, [running]);

  // useEffect(() => {
  //   // if (running) {
  //   setSpeed((prev) => prev + 1);
  //   // }
  // }, [running]);

  useEffect(() => {
    let val;
    if (running) {
      // setHeartRateVal(60);
      val = setInterval(() => {
        // setHeartRateVal((prevHR) => prevHR + 1);
        // setCaloriesVal((prevCal) => prevCal + 1);
        setSpeed((prev) => prev + 1);
      }, 10000);
    } else if (!running) {
      clearInterval(val);
    }
    return () => clearInterval(val);
  }, [running]);

  function getSpeed() {
    // distance = speed () * time
    let distance = 165;
    let time = 35;
    const total = distance / (time / 60);
    console.log(`speed: ${total}`);
    setSpeed(distance / (time / 60));

    // return speed;
  }

  function getDistance() {
    let distance = 165;
    let time = 35;
    const total = speed * (time / 60);

    console.log(`Distance: ${total}`);
    setDistance(speed * (time / 60));
  }

  function buttonControls() {}

  return (
    <div className="container">
      {/* <header>SMART TREADMILL</header> */}
      <div className="header">
        <Header title="SMART TREADMILL" />
      </div>
      <div className="time-bar">
        <TimeBar
          state={running}
          callTimeBarFunc={callTimeBarFunc}
          heart={heartRateVal}
          calories={caloriesVal}
        />
      </div>
      <div className="content">
        <div className="main">
          <CarouselView />
        </div>
        <div className="left-side">
          <SpeedBar title="Speed" value={speed.toFixed(2)} unit=" m/s" />
          {/* <SpeedBar title="Something" value={values} unit=" m/s" /> */}
        </div>
        <div className="right-side">
          <SpeedBar title="Distance" value={distance} unit=" km" />
          <div className="controlButtonContainer">
            <div
              className="rectangle"
              onClick={() => {
                setRunning(true);
              }}
              onTouchStart={() => {
                setRunning(true);
              }}
            >
              <h6>START</h6>
            </div>
            <div
              className="rectangle"
              onClick={() => {
                //buttonControls("profile");
                setRunning(false);
              }}
              onTouchStart={() => {
                setRunning(false);
              }}
            >
              <h6>STOP</h6>
            </div>
            <div className="rectangle" onClick={reset} onTouchStart={reset}>
              <h6>RESET</h6>
            </div>
          </div>
        </div>
      </div>
      <div className="footer">
        {/* <footer>FOOTER</footer> */}
        {/* <div
          className="rectangle"
          onClick={() => {
            controlModal("graph");
          }}
        >
          <h6>Graph</h6>
        </div>
        <div
          className="rectangle"
          onClick={() => {
            controlModal("profile");
          }}
        >
          <h6>Profile</h6>
        </div> */}
        <ChartView data={chartData} />

        <ModalView
          show={modalShow}
          onHide={() => setModalShow(false)}
          view={viewType}
        />
        {/* <div className="rectangle"></div> */}
      </div>
    </div>
  );
}

export default Dashboard;
