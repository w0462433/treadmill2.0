import "./App.css";
import ChartView from "./content/ChartView";
import Dashboard from "./Dashboard";

function App() {
  return (
    <div className="App">
      <Dashboard />
      {/* <ChartView /> */}
    </div>
  );
}

export default App;
